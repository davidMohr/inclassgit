<!DOCTYPE HTML>  
<html>
<head>
	<title><?= gettext("Master of Arts in Translation and Localization Management");?></title>
	<link rel="stylesheet" href="https://unpkg.com/purecss@1.0.0/build/base-min.css">
	<link rel="stylesheet" href="css/styles.css?v=1.0">
</head>
<body>  

<?php
session_start();

if (isset($_GET["locale"])) {
	$locale = $_GET["locale"];
	/*Append .UTF-8 automatically to all languages? */
}
else if (isset($_SESSION["locale"])) {
	$locale = $_SESSION["locale"];
}
else {
	$locale = "en_US";
}

putenv("LANG=" . $locale);
setlocale(LC_ALL, $locale);

$domain = "main";
bindtextdomain($domain, "./locale");
bind_textdomain_codeset($domain, 'UTF-8');

textdomain($domain);

$user = "TLM";
?>

	<h1><?= gettext("Master of Arts in Translation and Localization Management");?></h1>
	<p><strong><?= gettext("The Middlebury Institute Master of Arts in Translation and Localization Management prepares you to manage and implement localization projects in the global marketplace.");?></strong></p>
	<h2><?= gettext("What is Localization?");?></h2>
	<img src="https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.shutterstock.com%2Fsearch%2Ffun%2Btime&psig=AOvVaw3jUx6cIi0jsgPsFys5FkOG&ust=1605287351864000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCKis1MW__ewCFQAAAAAdAAAAABAI" alt="fun times" />
	<p><?= gettext("Localization is the process of adapting the content related to a product, service, or idea to the language and culture of a specific market or region. It’s a $40 billion industry and growing each year, making localization management a thriving field with excellent career prospects for students interested in language, culture, and technology.");?></p>
</body>
</html>